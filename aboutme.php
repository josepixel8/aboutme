<?php

// define the API class
class API {

    // function to print the full name
    public function printfullName($fullName) {
        echo "Full Name: $fullName\n";
    }

    // function to print the list of hobbies
    public function printHobbies($hobbies) {
        echo "<br>";
        echo "Hobbies:\n";
        echo "<br>";
        foreach ($hobbies as $hobby) {
            echo "\t$hobby\n";
            echo "<br>";
        }
    }

    // fucntion to print personal information
    public function printPersonalInformation($personalInfo) {
        echo "Age: {$personalInfo->age}\n";
        echo "<br>";
        echo "Email: {$personalInfo->email}\n";
        echo "<br>";
        echo "Birthday: {$personalInfo->birthday}\n";
    }
}

// create an instance of the API class
$api = new API();

// set the full name parameter & call the printFullname function
$fullName = "Jose Felipe Terdes";
$api->printFullName($fullName);

// set the hobbies parameter to an array and call the printHobbies function
$hobbies = array (
    "Watching Movie",
    "Playing Online Games",
    "Listening to Music"
);
$api->printHobbies($hobbies);

// set the personalInfo parameter to object and call the printPersonalInformation function
$personalInfo = (object) array (
    'age' => 21,
    'email' => 'josefelipe.terdes.pixel8@gmail.com',
    'birthday' => 'September 20, 2001'
);
$api->printPersonalInformation($personalInfo);

?>